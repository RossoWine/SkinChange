package com.zrq.skinchange.skin.attr;

import android.view.View;

/**
 * 找到需要换肤的view所需的属性
 */

public class SkinAttr {

    private String mResName;
    private SkinAttrType mType;


    public SkinAttr(String mResName, SkinAttrType mType) {
        this.mResName = mResName;
        this.mType = mType;
    }

    public SkinAttr() {
    }

    public String getResName() {

        return mResName;
    }

    public void setResName(String mResName) {
        this.mResName = mResName;
    }

    public SkinAttrType getType() {
        return mType;
    }

    public void setType(SkinAttrType mType) {
        this.mType = mType;
    }

    public void apply(View view) {
        mType.apply(view,mResName);

    }
}
