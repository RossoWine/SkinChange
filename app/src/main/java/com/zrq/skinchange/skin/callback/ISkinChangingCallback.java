package com.zrq.skinchange.skin.callback;

/**
 * Created by huwei on 2017/8/8.
 */

public interface ISkinChangingCallback {

    void onStart();

    void onError(Exception e);

    void onComplete();

    public static DefaultSkinChangeingCallback DEFAULT_CALLBACK=new DefaultSkinChangeingCallback();

    public class DefaultSkinChangeingCallback implements ISkinChangingCallback {

        @Override
        public void onStart() {

        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onComplete() {

        }
    }

}
