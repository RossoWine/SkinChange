package com.zrq.skinchange.skin.callback;

/**
 * Created by huwei on 2017/8/8.
 */

public interface ISkinChangeListener {
    void onSkinChanged();
}
