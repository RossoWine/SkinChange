package com.zrq.skinchange.skin.config;

/**
 * Created by huwei on 2017/8/8.
 */

public class Const {
    public static final String SKIN_PREFIX="skin_";
    public static final String PREF_NAME="skin_plugin";
    public static final String KEY_PLUGIN_PATH="key_plugin_path";
    public static final String KEY_PLUGIN_PKG="plugin_pkg";

    public static final String KEY_SUFFIX="plugin_suffix";
}
