package com.zrq.skinchange.skin.attr;

import android.content.Context;
import android.util.AttributeSet;

import com.zrq.skinchange.skin.config.Const;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public class SkinAttrSupport {

    public static List<SkinAttr> getSkinAttrs(AttributeSet attrs, Context context){

        List<SkinAttr> mSkinAttrs=new ArrayList<>();

        SkinAttrType attrType=null;
        SkinAttr skinAttr=null;
        //遍历获取每个attr的name和value
        for (int i = 0,n= attrs.getAttributeCount(); i <n ; i++) {

            String attrName=attrs.getAttributeName(i);
            String attrVal=attrs.getAttributeValue(i);

            //判断是否是引用资源
            if(attrVal.startsWith("@")){
                int id=-1;

                try {
                    id=Integer.parseInt(attrVal.substring(1));
                } catch (NumberFormatException e) {

                }
                if(id==-1){
                    continue;
                }

                String resName=context.getResources().getResourceEntryName(id);

                //判断是否是skin_开头
                if(resName.startsWith(Const.SKIN_PREFIX)){
                    //根据name找到type
                    attrType=getSupportAttrType(attrName);

                    if(attrType==null){
                        continue;
                    }

                    skinAttr=new SkinAttr(resName,attrType);
                    mSkinAttrs.add(skinAttr);
                }
            }

        }

        return mSkinAttrs;
    }

    private static SkinAttrType getSupportAttrType(String attrName) {

        for(SkinAttrType attrType:SkinAttrType.values()){
            if(attrType.getResType().equals(attrName)){
                return attrType;
            }
        }
        return null;

    }
}
