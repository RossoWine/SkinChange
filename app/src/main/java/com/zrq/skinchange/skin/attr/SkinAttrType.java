package com.zrq.skinchange.skin.attr;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrq.skinchange.skin.ResourceManager;
import com.zrq.skinchange.skin.SkinManager;

/**
 * 支持换肤的控件属性
 */

public enum  SkinAttrType {

    BACKGROUND("background"){
        @Override
        public void apply(View view, String resName) {

            Drawable drawable= getResourceManager().getDrawableByResName(resName);
            if(drawable!=null) {
                view.setBackgroundDrawable(drawable);
            }

        }
    },SRC("src") {
        @Override
        public void apply(View view, String resName) {
            Drawable drawable= getResourceManager().getDrawableByResName(resName);
            if(view instanceof ImageView){
                ImageView imageView= (ImageView) view;
                if(drawable!=null) {
                    imageView.setImageDrawable(drawable);
                }
            }

        }
    },Text_Color("textColor") {
        @Override
        public void apply(View view, String resName) {
            ColorStateList colorStateList= getResourceManager().getColorByResName(resName);
            if(view instanceof TextView){
                TextView textView= (TextView) view;
                if(colorStateList!=null){
                    textView.setTextColor(colorStateList);
                }
            }

        }
    };

    String resType;

    public String getResType() {
        return resType;
    }


    SkinAttrType(String type) {
        resType=type;
    }

    public abstract void apply(View view,String resName);

    public ResourceManager getResourceManager(){

        return SkinManager.getInstanc().getResourceManager();
    }

}
