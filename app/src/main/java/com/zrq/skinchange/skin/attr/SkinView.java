package com.zrq.skinchange.skin.attr;

import android.view.View;

import java.util.List;

/**
 * 需要换肤的view集合
 */

public class SkinView {

    private View mView;
    private List<SkinAttr> mAttrs;

    public SkinView() {
    }

    public View getView() {

        return mView;
    }

    public void setView(View mView) {
        this.mView = mView;
    }

    public List<SkinAttr> getAttrs() {
        return mAttrs;
    }

    public void setAttrs(List<SkinAttr> mAttrs) {
        this.mAttrs = mAttrs;
    }

    public SkinView(View view, List<SkinAttr> attrs) {
        this.mView = view;
        this.mAttrs = attrs;

    }

    public void apply(){
        for(SkinAttr attr:mAttrs){
            attr.apply(mView);
        }
    }
}
