package com.zrq.skinchange.skin;

import android.app.Application;

/**
 * Created by huwei on 2017/8/8.
 */

public class SkinApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SkinManager.getInstanc().init(this);
    }
}
