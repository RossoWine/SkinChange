package com.zrq.skinchange.skin;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;



public class ResourceManager {
    private Resources mResources;
    private String mPkgName;
    private String mSuffix;

    public ResourceManager(Resources resources, String pkgName,String suffix) {
        this.mResources = resources;
        this.mPkgName = pkgName;

        if(suffix==null){
            suffix="";
        }
        mSuffix=suffix;
    }
    public Drawable getDrawableByResName(String name){
        name=appendSuffix(name);
        try{
            return  mResources.getDrawable(mResources.getIdentifier(name,"drawable",mPkgName));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private String appendSuffix(String name) {
        if(!TextUtils.isEmpty(mSuffix)){
            name+="_"+mSuffix;
        }

        return name;
    }

    public ColorStateList getColorByResName(String name){
        try{
            name=appendSuffix(name);
            return  mResources.getColorStateList(mResources.getIdentifier(name,"color",mPkgName));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
