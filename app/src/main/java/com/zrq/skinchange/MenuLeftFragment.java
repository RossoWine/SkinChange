package com.zrq.skinchange;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zrq.skinchange.skin.SkinManager;

public class MenuLeftFragment extends Fragment implements View.OnClickListener{

    private View mRedView;
    private View mGreenView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRedView = view.findViewById(R.id.id_rl_innerchange01);
        mGreenView = view.findViewById(R.id.id_rl_innerchange02);

        mRedView.setOnClickListener(this);
        mGreenView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.id_rl_innerchange01:
                SkinManager.getInstanc().changeSkin("red");
                break;
            case R.id.id_rl_innerchange02:
                SkinManager.getInstanc().changeSkin("green");
                break;
        }
    }
}
