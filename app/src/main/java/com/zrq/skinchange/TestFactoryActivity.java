package com.zrq.skinchange;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.zrq.skinchange.skin.base.BaseSkinActivity;

public class TestFactoryActivity extends BaseSkinActivity {

    private static final String TAG = "TestFactoryActivity";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //使用LayoutInflaterCompat解决版本兼容问题，，21，，，11
        LayoutInflaterCompat.setFactory(mInflater, new LayoutInflaterFactory() {
            @Override
            public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
                Log.e(TAG, "name");
                for (int i = 0, n = attrs.getAttributeCount(); i < n; i++) {

                    String attrName = attrs.getAttributeName(i);
                    String attrVal = attrs.getAttributeValue(i);
                    Log.e(TAG, attrName + "=" + attrVal);
                }
                /**
                 * 控制在布局文件标签的view的生成
                 */
                if (name.equals("TextView")) {
                    return new EditText(context, attrs);
                }
                return null;
            }
        });

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_factory);

    }

}
