package com.zrq.skinchange;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.view.ViewHelper;
import com.zrq.skinchange.skin.base.BaseSkinActivity;
import com.zrq.skinchange.skin.callback.ISkinChangingCallback;
import com.zrq.skinchange.skin.SkinManager;

import java.io.File;

/**
*MainActivity
*/
public class MainActivity extends BaseSkinActivity
{
    /**
     * 这是一个MainActivity
     */

/**
*这是一个Activity
*/
    private DrawerLayout mDrawerLayout;
    private ListView mListView;
    private String[] mDatas = new String[]{"Activity", "Service", "Activity", "Service", "Activity", "Service", "Activity", "Service"};


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initEvents();

    }

    private void initEvents() {
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                View mContent = mDrawerLayout.getChildAt(0);
                View mMenu=drawerView;
                float scale =1-slideOffset;
                float rightScale=0.8f+scale*0.2f;
                if(drawerView.getTag().equals("LEFT")){
                    float leftScale=1-0.3f*scale;

                    ViewHelper.setScaleX(mMenu,leftScale);
                    ViewHelper.setScaleY(mMenu,leftScale);
                    ViewHelper.setAlpha(mMenu,0.6f+0.4f*(1-scale));
                    ViewHelper.setTranslationX(mContent,
                            mMenu.getMeasuredWidth()*(1-scale));
                    ViewHelper.setPivotX(mContent,0);
                    ViewHelper.setPivotY(mContent,
                            mContent.getMeasuredHeight()/2);
                    mContent.invalidate();
                    ViewHelper.setScaleX(mContent,rightScale);
                    ViewHelper.setScaleY(mContent,rightScale);

                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }


    private void initView()
    {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.id_drawerLayout);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.id_left_menu_container);
        if (fragment == null)
        {
            fm.beginTransaction().add(R.id.id_left_menu_container, new MenuLeftFragment()).commit();
        }
        mListView= (ListView) findViewById(R.id.id_listview);
        mListView.setAdapter(new ArrayAdapter<String>(this,-1,mDatas){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                if(convertView==null){
                    convertView=LayoutInflater.from(MainActivity.this).inflate(R.layout.item,parent,false);
                }
                TextView tv= (TextView) convertView.findViewById(R.id.id_tv_title);
                tv.setText(getItem(position));
                return convertView;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    private String mSkinPluginPath= Environment.getExternalStorageDirectory()+ File.separator+"skin_plugin.apk";
    private String mSkinPluginPKg="com.example.skin_plugin";
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){
            case R.id.action_changeSkin:

//                loadPlugin(mSkinPluginPath,mSkinPluginPKg);
                SkinManager.getInstanc().changeSkin(mSkinPluginPath,mSkinPluginPKg, new ISkinChangingCallback() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onError(Exception e) {
//                        Toast.makeText(MainActivity.this, "换肤失败", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Toast.makeText(MainActivity.this, "换肤成功", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
            case R.id.action_testFactory:
                Intent intent=new Intent(this,TestFactoryActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
